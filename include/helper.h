#include "hashtable.h"
#include "csapp.h"
int iniciarServidor(char *ip,int puerto);
int buscarFinLinea(int fd, char *linea);
void reemplazarCaracter(char *str, char viejo, char nuevo);
void llenarHashtable(hashtable *indices, char *archivo);
char ** split(char * str, char * delimiter, int size);
char * concatenarRutaBase(char * nombre_db, char * prefijo);
char * concatenarRutaIndicesBase(char * nombre_db, char * prefijo);
void copiarArchivo(char * rutaOriginal, char * rutaCopia);
int send_all(int socket, const void *buffer, size_t length, int flags);
int open_clientfd(char *hostname, int port) ;
