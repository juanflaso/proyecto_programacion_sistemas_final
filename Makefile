#Escriba su makefiile en este archivo
#
# Luiggi Alarcon Gonzabay
# Juan Laso Delgado

#Si quieren llamar reglas adicionales, agregenlas como pre-requisitos a la regla all
#La regla logdb.so deben modificarla con su comando para generar dicho ejecutable.
#El archivo prueba.c esta vacio. NO deben llenarlo. Este se reemplazara con el codigo del profesor
#al probar su proyecto

FLAGS= gcc -Wall -c -I

ARCHIVOSLOGDB = src/conectar_db.c src/abrir_db.c src/cerrar_db.c src/compactar.c src/crear_db.c src/eliminar.c src/get_val.c src/put_val.c

ARCHIVOSHASH = src/hash.c src/crearHashtable.c src/numeroElementos.c src/put.c src/get.c src/remover.c src/borrar.c src/claves.c src/contieneClave.c src/valores.c

ARCHIVOSHELPER = src/iniciarServidor.c src/llenarHashtable.c src/concatenarRutaBase.c src/concatenarRutaIndicesBase.c src/reemplazarCaracter.c src/buscarFinLinea.c src/split.c src/copiarArchivo.c src/send_all.c src/open_clientfd.c

all:  lib/libhashtab.so bin/prueba bin/logdb bin/prueba_est
	
bin/logdb:  obj/logdb.o lib/libhelper.so lib/libcsapp.so
	gcc -Wall obj/logdb.o -lhelper -lhashtab -lcsapp -Llib/ -o $@ -g
obj/logdb.o:src/logdb.c
	$(FLAGS) include/ $< -o $@ -g

bin/prueba_est: obj/prueba_est.o lib/libhelper.so lib/liblogdb.so
	gcc -Wall obj/prueba_est.o -llogdb -lhelper -lhashtab -Llib/ -o $@
obj/prueba_est.o: src/prueba_est.c
	gcc -Wall -Iinclude/ -c src/prueba_est.c -o obj/prueba_est.o

bin/prueba: obj/prueba.o lib/liblogdb.so
	gcc obj/prueba.o -lhashtabprof -llogdb -Llib/ -o bin/prueba
obj/prueba.o: src/prueba.c
	gcc -Wall -Iinclude/ -c src/prueba.c -o obj/prueba.o



#librerias.so
lib/liblogdb.so:
	gcc -Wall -fPIC -shared -Iinclude/ $(ARCHIVOSLOGDB) -o lib/liblogdb.so

lib/libhashtab.so:
	gcc -Wall -fPIC -shared -Iinclude/ $(ARCHIVOSHASH) -o $@

lib/libhelper.so:
	gcc -Wall -fPIC -shared -Iinclude/ $(ARCHIVOSHELPER) -o $@

lib/libcsapp.so:
	gcc -Wall -fPIC -shared -Iinclude/ src/open_clientfd.c -o lib/libcsapp.so

.PHONY: clean
clean:
	rm bin/* obj/* lib/libhashtab.so lib/libhelper.so lib/liblogdb.so

