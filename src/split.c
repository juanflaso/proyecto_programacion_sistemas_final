#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "helper.h"

char ** split(char * str, char * delimiter, int size) {
    
    char ** arr = malloc(sizeof(char *)*(size));
    char * buf ;
    
    buf = strtok(str, delimiter);
    
    int count = 0;
    while (buf != NULL) {
        //printf ("%s\n",buf);
        arr[count] = buf;
        buf = strtok (NULL, "\n");
        count ++;
        
    }
    
    return arr;
}