#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"
char **claves(hashtable *tabla, int *conteo){
    int numElementos = tabla->elementos;
	*conteo = numElementos;
	if(numElementos == 0)
		return NULL;
    char **arr = malloc(sizeof(char*)*numElementos);

    if (arr == NULL) {
        return NULL;
    }

  
    int numBuckets = tabla->numeroBuckets;
    objeto* bucket;
    int objActual = 0;
    for(int i = 0; i < numBuckets; i++){
        bucket = (tabla->buckets)[i];
        while(bucket != NULL){
            arr[objActual] = bucket->clave;
            objActual++;
            bucket = bucket->siguiente;
        }
        
    }
    return arr;
}