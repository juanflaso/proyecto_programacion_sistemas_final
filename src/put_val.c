#include "logdb.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

int put_val(conexionlogdb *conexion, char *clave, char *valor){
	int sockfd = conexion->sockdf;
	char *linea = (char *) malloc(strlen(clave)+strlen(valor)+2); //Verificar el tamano del string con +2 por los ":" y el salto de linea
	if(linea==NULL)
		return 0;

	strcpy(linea,clave);
	strcat(linea,":");
	strcat(linea,valor);
	strcat(linea,"\n");
		
	
	int bytesSent = send(sockfd,linea,strlen(linea),0); //enviamos la solicitud al servidor
	free(linea);
	if (bytesSent  < 0){
		printf("Error al enviar la solicitud\n");
		return 0;
	}
	else{
		printf("Request enviado correctamente\n");
	}
	
	char buf[100];
	int bytesRecieved = recv(sockfd, buf, 100, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta\n");
		return 0;
	}
	else{
		printf("Respuesta recibida con exito (Clave escrita)\n");
		if(strcmp(buf,"error")==0){
			printf("No se pudo escribir la clave\n");
			return 0;
		}
	}
	return 1;
}