#include <string.h>
#include <stdlib.h>

char * concatenarRutaBase(char * nombre_db, char * prefijo) {
    char * ruta = "./../bases/";
    char * extension = ".txt";

    char * rutaFinal;
    if((rutaFinal = malloc(strlen(ruta)+strlen(prefijo)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinal[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinal,ruta);
        strcat(rutaFinal, prefijo);
        strcat(rutaFinal,nombre_db);
        strcat(rutaFinal,extension);
    }
    
    return rutaFinal;
}