#include <stdio.h>
#include "hashtable.h"
int contieneClave(hashtable *tabla, char *clave){
    int hashedKey =hash((unsigned char *)clave)%(tabla->numeroBuckets);
    objeto *bucketTmp;
    bucketTmp = tabla->buckets[hashedKey];
    while (bucketTmp != NULL) {
        if (strcmp(bucketTmp->clave, clave) == 0) {
            return 1;
        }
        bucketTmp=bucketTmp->siguiente;
    }
    return 0;
}