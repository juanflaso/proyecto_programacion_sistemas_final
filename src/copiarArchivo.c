#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void copiarArchivo(char * rutaOriginal, char * rutaCopia) {

    FILE* original = fopen(rutaOriginal,"r");
	FILE* copia = fopen(rutaCopia, "w+");

    if (original==NULL||copia == NULL){
        return;
    }

    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, original)) != -1) {
        fputs(line, copia);
    }

    fclose(original);
    fclose(copia);

    if (line)
        free(line);
}