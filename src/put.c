#include <stdio.h>
#include <string.h>
#include "hashtable.h"

void put(hashtable *tabla, char *clave, void *valor) {
    int hashedKey =hash((unsigned char *)clave)%(tabla->numeroBuckets);
    objeto **buckets = tabla->buckets;
    objeto *newBucket = malloc(sizeof(objeto));
    
    if(newBucket == NULL) {
        return;
    }
	newBucket->clave = malloc(sizeof(char)*strlen(clave) + 1);
	
	if(newBucket->clave == NULL) {
        return;
    }
    strcpy(newBucket->clave, clave);
    newBucket->valor = valor;
    newBucket->siguiente = NULL;
	
    if(buckets[hashedKey] == NULL) {
        (buckets[hashedKey]) = newBucket;
    }
    else {
        objeto *bucket;
        bucket = buckets[hashedKey];
		objeto *bucketTmp=bucket;
        while (bucketTmp != NULL) {
            if (strcmp(bucketTmp->clave, clave) == 0) {
                bucketTmp->valor = strdup((void*)valor);
				//printf("REPETIDOS\nclave: %s - valor: %s\n",bucketTmp->clave,(char *)bucketTmp->valor);
                return;
            }else if(bucketTmp->siguiente ==NULL){
				bucket=bucketTmp;
			}
			bucketTmp=bucketTmp->siguiente;
        }
        bucket->siguiente = newBucket;
    }
    tabla->elementos = tabla->elementos + 1;
}
