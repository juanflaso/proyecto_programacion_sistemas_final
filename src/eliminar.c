#include "logdb.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

int eliminar(conexionlogdb *conexion, char *clave){
	int sockfd = conexion->sockdf;
	char *linea = (char *) malloc(strlen(clave)+strlen("@T!")); //Verificar el tamano del 		string con +2 por los ":" y el salto de linea
	strcpy(linea,clave);
	strcat(linea,":");
	strcat(linea,"@T!\n");
		
	int bytesSent = send(sockfd,linea,strlen(linea),0); //enviamos la solicitud al servidor
	free(linea);

	if (bytesSent  < 0){
		printf("Error al enviar la solicitud\n");
		return 0;
	}
	else{
		printf("Request enviado correctamente\n");
	}
	
	char buf[100];
	int bytesRecieved = recv(sockfd, buf, 100, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta\n");
		return 0;
	}
	else{
		printf("Respuesta recibida con exito");
	}
	return 1;
}
