#include "helper.h"
#include <stdio.h>
#include <stdlib.h>       
#include <stddef.h>
#include <unistd.h>           
#include <string.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <fcntl.h>

int buscarFinLinea(int fd, char *linea){
	int cnt=0;
	char letra;
	while(cnt<100){
		read(fd,&letra,1);
		linea[cnt] = letra;
		cnt++;
		if (letra=='\n'){
			reemplazarCaracter(linea,'\n',0);
			return cnt;
		}
	}
	return cnt;
}
