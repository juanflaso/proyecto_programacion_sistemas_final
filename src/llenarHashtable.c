#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hashtable.h"
#include "helper.h"

#define BUF_LEN 100 

void llenarHashtable(hashtable *indices, char *archivo){
	char buf[BUF_LEN];
	char *clave;
	char *valor;
	FILE *fileIndices = fopen(archivo, "r");
	while(fgets(buf, BUF_LEN, fileIndices) != NULL){
		reemplazarCaracter(buf,'\n', 0);
		clave = strtok(buf,":");
		valor = strtok(NULL,":");
		put(indices , strdup(clave) , strdup(valor));
		memset(buf,0, BUF_LEN);
	}
	fclose(fileIndices);
	printf("hashtable llena\n");
}
