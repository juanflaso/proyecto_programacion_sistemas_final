#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hashtable.h"

void *remover(hashtable *tabla, char *clave) {
    int hashedKey = hash((unsigned char *)clave)%(tabla->numeroBuckets);
    objeto **buckets = tabla->buckets;
    if(buckets[hashedKey] == NULL) {
        return NULL;
    }
    objeto *bucket;
    bucket = buckets[hashedKey];
    if (strcmp(bucket->clave, clave) == 0) {
        tabla->elementos = tabla->elementos-1;
        void *objetoARetornar = bucket->valor;
        if(bucket->siguiente == NULL){
            free(bucket->clave);
            bucket->clave = NULL;
            free(bucket);
            bucket = NULL;
            buckets[hashedKey] = NULL;
            return objetoARetornar;
        }
        buckets[hashedKey] = bucket->siguiente;
        free(bucket->clave);
        bucket->clave = NULL;
        free(bucket);
        bucket = NULL;
        return objetoARetornar;         
    }

    objeto *sigBucket;
    while (bucket->siguiente != NULL) {
        sigBucket = bucket->siguiente;
        if (strcmp(sigBucket->clave, clave) == 0) {
            tabla->elementos = tabla->elementos-1;
            void *objetoARetornar = sigBucket->valor;
            bucket->siguiente = sigBucket->siguiente;
            free(sigBucket->clave);
            sigBucket->clave = NULL;
            sigBucket->siguiente = NULL;
            sigBucket->valor = NULL;
            free(sigBucket);
            sigBucket = NULL;
            return objetoARetornar;
        }
        bucket = sigBucket;
    }
    return NULL;
}