#include "logdb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

void cerrar_db(conexionlogdb *conexion){
    char * opcion="CLOSEDATABASE:CERRAR";
 

	int bytesSent = send(conexion->sockdf,opcion,strlen(opcion)+1,0); //enviamos la solicitud de crear una base al servidor
	if (bytesSent  < 0){
		printf("Error al enviar la solicitud de cerrar base\n");
		return;
	}
	else{
		printf("Request enviado correctamente de cerrar base\n");
	}
	char buf[100];
	int bytesRecieved = recv(conexion->sockdf, buf, 100, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta de cerrar base\n");
		return;
	}
	else{
		printf("Respuesta recibida con exito\n");
		if(strcmp(buf,"error")==0){
			printf("No se pudo cerrar la base de datos solicitada\n");
			return;
		
		}
	}

    close(conexion->sockdf);
    conexion->sockdf = 0;
    conexion->ip = NULL;
    conexion->puerto = 0;
    conexion->sockdf = 0;
    conexion->id_sesion = 0;
    conexion->nombredb = NULL;
    free(conexion);
	
}
