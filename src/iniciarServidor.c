#include <sys/types.h> 
#include <sys/stat.h>
#include <stdio.h>          
#include <stdlib.h>       
#include <stddef.h>           
#include <string.h>           
#include <unistd.h>            
#include <signal.h>          
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include <sys/wait.h>

int iniciarServidor(char *ip, int puerto){
	int sockfd;
	char *direccionIP = ip;
	struct sockaddr_in servidorAddr;
	memset(&servidorAddr, 0, sizeof(servidorAddr)); //valores de la estructura en 0
	servidorAddr.sin_family = AF_INET;	
	servidorAddr.sin_port = htons(puerto);		
	servidorAddr.sin_addr.s_addr = inet_addr(direccionIP) ;
		   
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
		printf("Error al iniciar el servidor\n");
		return -1;
	}
	if((bind(sockfd , (struct sockaddr *)&servidorAddr , sizeof(servidorAddr))) < 0){
		printf("Error al iniciar el servidor\n");
		return -1;		
	}
	if (listen(sockfd,1024) < 0){
		printf("Error al iniciar el servidor\n");
		return -1;
	}
	return sockfd;
}
