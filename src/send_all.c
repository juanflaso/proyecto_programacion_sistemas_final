#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>

int send_all(int socket, const void *buffer, size_t length, int flags){
    ssize_t n;
    const char *p = buffer;
    while (length > 0)
    {
        n = send(socket, p, length, flags);
        if (n <= 0)
            return 0;
        p += n;
        length -= n;
    }
    return 1;
}