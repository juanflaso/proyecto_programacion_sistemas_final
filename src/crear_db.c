#include "logdb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

int crear_db(conexionlogdb *conexion, char *nombre_db) {
    char * opcion="CREATEDATABASE:";
    char * ruta = "./../bases/";
    char * rutaIndices = "./../bases/indice_";
    char * extension = ".txt";

    printf("CREANDO BASE\n");

    char * rutaFinal;
    char *rutaFinalIndice;
    if((rutaFinal = malloc(strlen(opcion)+strlen(ruta)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinal[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinal,opcion);
        strcat(rutaFinal,ruta);
        strcat(rutaFinal,nombre_db);
        strcat(rutaFinal,extension);
    } 
    if((rutaFinalIndice = malloc(strlen(rutaIndices)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinalIndice[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinalIndice,rutaIndices);
        strcat(rutaFinalIndice,nombre_db);
        strcat(rutaFinalIndice,extension);
    }
    else {
	printf("NO SE PUDO CREAR LA BASE\n");
        return 0;
    }
	//int rutaFinalFD = open(nombredb, O_CREAT);
    //int rutaFinalIndiceFD = open(nombre, O_CREAT);

    //if(rutaFinalFD<0 || rutaFinalIndiceFD<0) {
      //  return 0;
   // }
	//close(rutaFinalFD);
    //close(rutaFinalIndiceFD);
	int bytesSent = send(conexion->sockdf,rutaFinal,strlen(rutaFinal)+1,0); //enviamos la solicitud de crear una base al servidor
	if (bytesSent  < 0){
		printf("Error al enviar la solicitud de crear base\n");
		return 0;
	}
	else{
		printf("Request enviado correctamente de crear base\n");
	}
	
	char buf[100];
	int bytesRecieved = recv(conexion->sockdf, buf, 100, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta de creacion de base\n");
		return 0;
	}
	else{
		printf("Respuesta recibida con exito\n");
		if(strcmp(buf,"error")==0){
			printf("No se pudo crear la base de datos solicitada\n");
			return 0;
		
		}
	}


  
    //TODO int server = iniciarServidor(conexion->ip, conexion->puerto, rutaFinal, rutaFinalIndice);
    free(rutaFinal);
    free(rutaFinalIndice);
    printf("BASE CREADA\n");
    return 1;
    //TODO return server;
}
