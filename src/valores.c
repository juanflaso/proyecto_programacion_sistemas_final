#include <stdio.h>
#include "hashtable.h"
void **valores(hashtable *tabla, int *conteo){
    int numElementos = tabla->elementos;
	*conteo = numElementos;
	if(numElementos == 0)
		return NULL;
    void **arr = malloc(sizeof(void*)*numElementos);

    if (arr == NULL) {
        return NULL;
    }

    
    int numBuckets = tabla->numeroBuckets;
    objeto* bucket;
    int objActual = 0;
    for(int i = 0; i < numBuckets; i++){
        bucket = (tabla->buckets)[i];
        while(bucket != NULL){
            arr[objActual] = bucket->valor;
            objActual++;
            bucket = bucket->siguiente;
        }
        
    }
    return arr;
}