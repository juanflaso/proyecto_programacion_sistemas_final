
#include "logdb.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "csapp.h"


conexionlogdb *conectar_db(char *ip, int puerto) {
    conexionlogdb *conex = malloc(sizeof(conexionlogdb));

    int clientfd = open_clientfd(ip, puerto);

    if(clientfd == -1) {
        return NULL;
    }

    conex->ip=ip;
    conex->puerto = puerto;
    conex->sockdf = clientfd;
    srand(time(NULL)); 
    conex->id_sesion =  rand();

    return conex;
}