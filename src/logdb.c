#include "hashtable.h"
#include "logdb.h"
#include "helper.h"

#include <sys/types.h> 
#include <sys/stat.h>
#include <stdio.h>          
#include <stdlib.h>       
#include <stddef.h>           
#include <string.h>           
#include <unistd.h>            
#include <signal.h>          
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>
#include <sys/wait.h>

#define BUFLEN 100
#define NUMBUCKETS 10

//funcion para reemplazar el salto de linea por final de linea "\n" con 0


int main( int argc, char *argv[]) {
	int sockfd;
	if(argc <3){
		printf("Uso: ./logdb <dir> <ip> <puerto>\n");
		exit(-1);
	}
	//char *dir = argv[1];
	char* direccionIP = argv[2];
	int puerto = atoi(argv[3]);
	
	if ( (sockfd = iniciarServidor(direccionIP,puerto) )<0){ //inicializando un servidor
		printf("Error al iniciar el servidor\n");
		perror("ERROR:");
		exit(1);
	}
	
	//Aqui se va a recibir cual archivo de base de datos se quiere crear
	/*
	char nombredb[BUFLEN];
	memset(nombredb,0,BUFLEN);
	int n,clfd;
	
	printf("esperando por nombre \n");
	clfd = accept(sockfd, NULL, NULL); //primer request que recibe que es el nombre de la base de datos
	if ( (n = recv(clfd,nombredb,100,0) ) < 0){
		printf("Error al recibir el nombre de la base\n");
		exit(1);	
	}
	close(clfd);
	printf("nombre recibido: %s \n",nombredb);
	
	char *rutaFinalIndice = concatenarRutaIndicesBase(nombredb,""); //se consigue la ruta indice de la base
	//se crea la hashtable cuando se pueda iniciar un servidor con exito
	int fdIndices = open(rutaFinalIndice,O_WRONLY|O_APPEND|O_CREAT,0666);
	close(fdIndices);
	hashtable *indices = crearHashTable(NUMBUCKETS);
	
	if ( indices == NULL)
		exit(1);
	
	printf("llenando hashtable\n");
	llenarHashtable(indices,rutaFinalIndice);
	//hashtable indices llena luego de leer el archivo rutaFinalIndice
	
	char *rutaFinalBase = concatenarRutaBase(nombredb,"");
	
	char buffer[BUFLEN];
	char linea[BUFLEN];
	int fd = open(rutaFinalBase,O_WRONLY|O_APPEND|O_CREAT,0666);
	if(fd < 0){
		perror("Error: ");
		return 0;
	}
	*/
	
	char buffer[BUFLEN];
	char linea[BUFLEN];
	memset(linea,0,BUFLEN);
	memset(buffer,0,BUFLEN);
	int fdBase=-1;
	int fdIndices=-1;
	int bytesSent;
	int clfd2=-1;
	hashtable *indices;
 	char *rutaFinalIndices;
	char *clave;
	char *valor;
	while(1){
		if(clfd2 == -1){
			printf("Esperando conexion de un cliente\n");
			clfd2 = accept(sockfd, NULL, NULL);
			printf("Cliente conectado\n");
			bytesSent = send(clfd2,"exito",strlen("exito"),0);
			if(bytesSent<0){
				printf("Servidor: Error al enviar respuesta\n");
			}
			printf("Servidor: Respuesta enviada correctamente\n");

		}
		printf("recibiendo señal\n");
		int n = recv(clfd2,buffer,BUFLEN,0);
		
		if(n==0){ //cliente cerro la conexion
			close(clfd2);
			clfd2=-1;
			continue;
		}
		else{
			strcpy(linea,buffer); // linea = clave:valor
			
			char **arr= split(buffer,":",2);
			clave = arr[0];
			valor = arr[1];
			//char *clave=strtok(buffer,":");
			//char *valor=strtok(NULL,":");
			if(strcmp(clave,"CREATEDATABASE")==0){
				printf("SOLICITUD PARA CREAR BASE DE DATO recibida\n");
				printf("Nombre de la base a crear recibido: %s \n",valor);
				int rutaFinalFD = open(valor, O_CREAT,DEF_MODE);

				if(rutaFinalFD<0) {
					printf("Error al crear la base de datos\n");
					send(clfd2,"error",strlen("error"),0);
				}else{
					printf("Servidor: Base de datos creada exitosamente\n");
					send(clfd2,"exito",strlen("exito"),0);
				}
				close(rutaFinalFD);	
				
			}else if(strcmp(clave,"OPENDATABASE") == 0) {
				printf("SOLICITUD PARA ABRIR BASE DE DATO recibida\n");
				printf("Nombre de la base que se va abrir recibido: %s \n",valor);
				rutaFinalIndices = concatenarRutaIndicesBase(valor,""); //se consigue la ruta indice de la base
				//se crea la hashtable cuando se pueda iniciar un servidor con exito
				printf("ruta indices %s\n",rutaFinalIndices);
				fdIndices = open(rutaFinalIndices,O_WRONLY|O_APPEND|O_CREAT,0666);
				if(fdIndices <0){
					printf("ERROR AL ABRIR EL ARCHIVO INDICES\n");
					exit(1);
				}
				close(fdIndices);
				indices = crearHashTable(NUMBUCKETS);

				if ( indices == NULL){
					exit(1);
				}
				printf("Llenando hashtable desde el archivo %s\n",rutaFinalIndices);
				llenarHashtable(indices,rutaFinalIndices);
				//char *prueba1=get(indices,"reloj");
				//printf("%s\n",prueba1);
				char *rutaFinalBase = concatenarRutaBase(valor,"");
				fdBase = open(rutaFinalBase,O_WRONLY|O_APPEND|O_CREAT,0666);
				if(fdBase < 0){
					send(clfd2,"error",strlen("error"),0);
				}else{
					printf("BASE DE DATOS %s abierta con exito\n",valor);
					send(clfd2,"exito",strlen("exito"),0);
				}
			
			}else if(strcmp(clave,"CLOSEDATABASE") == 0){
				printf("SOLICITUD PARA CERRAR BASE DE DATO recibida\n");
				if(fdBase < 0){
						printf("Servidor: no ha abierto la base de datos aun\n");
						send(clfd2,"error",strlen("error"),0);
				}else{
					
					int i,tamano;
					tamano = indices->numeroBuckets;
					objeto **buckets = indices->buckets;
					fdIndices = open(valor,O_WRONLY|O_TRUNC|O_CREAT,0666);
					if(fdIndices < 0){
						send(clfd2,"error",strlen("error"),0);
					}else{
						
					
						for(i=0 ; i < tamano ; i++){
							objeto *bucket;
							bucket=buckets[i];
							char *linea2;
							if (bucket != NULL){
								if ( bucket->siguiente == NULL){
									char *claveIndice = bucket->clave;
									char *valorIndice = bucket->valor;
									linea2 = (char *) malloc(strlen(claveIndice)+strlen(valorIndice)+2); //Verificar el tamano del string con +2 por los ":" y el salto de linea
									if(linea2==NULL)
										return 0;

									strcpy(linea2,clave);
									strcat(linea2,":");
									strcat(linea2,valor);
									strcat(linea2,"\n");
									bytesSent = write(fdIndices,linea2,BUFLEN); //escribimos en el archivo
									if(bytesSent<0){
										send(clfd2,"error",strlen("error"),0);
									}

									free(bucket->clave);
									bucket->clave = NULL;
									bucket->valor = NULL;
									free(bucket);
									bucket = NULL;
									indices->elementos = indices->elementos-1;
								}
								else{
									objeto *bucketTmp;

									while(bucket->siguiente != NULL){

										bucketTmp = bucket->siguiente;
										bucket->siguiente = bucketTmp->siguiente;

										char *claveIndice = bucketTmp->clave;
										char *valorIndice = bucketTmp->valor;
										linea2 =(char *) malloc(strlen(claveIndice)+strlen(valorIndice)+2); //Verificar el tamano del string con +2 por los ":" y el salto de linea
										if(linea2==NULL)
											return 0;

										strcpy(linea2,clave);
										strcat(linea2,":");
										strcat(linea2,valor);
										strcat(linea2,"\n");
										bytesSent = write(fdIndices,linea2,BUFLEN); //escribimos en el archivo
										if(bytesSent<0){
											send(clfd2,"error",strlen("error"),0);
										}
										free(bucketTmp->clave);
										bucketTmp->clave = NULL;
										bucketTmp->valor = NULL;
										bucketTmp->siguiente = NULL;
										free(bucketTmp);
										bucketTmp = NULL;
										indices->elementos = indices->elementos-1;
									}
									char *claveIndice = bucket->clave;
									char *valorIndice = bucket->valor;
									linea2 =(char *) malloc(strlen(claveIndice)+strlen(valorIndice)+2); //Verificar el tamano del string con +2 por los ":" y el salto de linea
									if(linea2==NULL)
										return 0;

									strcpy(linea2,clave);
									strcat(linea2,":");
									strcat(linea2,valor);
									strcat(linea2,"\n");
									bytesSent = write(fdIndices,linea2,BUFLEN); //escribimos en el archivo
									if(bytesSent<0){
										send(clfd2,"error",strlen("error"),0);
									}
									free(bucket->clave);
									bucket->clave = NULL;
									bucket->valor = NULL;
									free(bucket);
									bucket = NULL;
									indices->elementos = indices->elementos-1;
								}
							}
						}
					}
					printf("BASE DE DATOS cerrada con exito\n");
					//char *prueba1=get(indices,"reloj");
					//printf("reloj: %s\n",prueba1);
					close(clfd2);
					close(fdIndices);
					close(fdBase);
					clfd2=-1;
				}
			
			}else{
				printf("REQUESTS DE PUT-GET-ELIMINAR\n");
				//printf("clave:%s - valor:%s\n",clave,valor);
				if (valor!=NULL){ //si el cliente envia valor entonces hace request de put
					if(fdBase < 0){
						printf("Error: no ha abierto la base de datos aun\n");
					}else{
						int indiceFinal = lseek(fdBase,0,SEEK_END);
						char offset[BUFLEN];
						sprintf(offset,"%d",indiceFinal);
						if(strcmp(valor,"@T!")==0){
							remover(indices,clave);
							bytesSent = write(fdBase,linea,strlen(linea)); //escribimos en el archivo
							if(bytesSent<0){
								send(clfd2,"error",strlen("error"),0);
							}
						}else{
							//printf("linea = %s\n",linea);
							put(indices,clave,offset); //agregamos al hashtable indices
							bytesSent = write(fdBase,linea,strlen(linea)); //escribimos en el archivo
							if(bytesSent<0){
								send(clfd2,"error",strlen("error"),0);
							}else{
								printf("Clave ingresada con exito\n");
								send(clfd2,"exito",strlen("exito"),0);
							}
						}
					}
				memset(clave,0,strlen(clave));
				memset(valor,0,strlen(valor));
				}else{  //si solo envia una palabra entonces quiere hacer request de get
					if(fdBase < 0){
						printf("Error: no ha abierto la base de datos aun\n");
					}else{
						int indice =atoi((char *)get(indices,clave)); //cambios *
						if (indice == 0){ //si no hay la clave envia un NULL
							send(clfd2,"error",0,0);
						}
						lseek(fdBase,indice+strlen(clave)+1,SEEK_SET); //empezamos a leer despues de la clave
						char respuestaValor[BUFLEN];
						int i = buscarFinLinea(fdBase,respuestaValor);
						send(clfd2,respuestaValor,i,0);
					}
				}
				memset(clave,0,strlen(clave));
				memset(valor,0,strlen(valor));
			}
		}	
		memset(buffer,0,strlen(buffer));
		memset(linea,0,strlen(linea)); 
		
	}
}

	
