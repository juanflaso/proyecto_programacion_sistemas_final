#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "logdb.h"
#include "helper.h"

int abrir_db(conexionlogdb *conexion, char *nombre_db){
    char * opcion="OPENDATABASE:";
    char * ruta = "./../bases/";
    char * rutaIndices = "./../bases/indice_";
    char * extension = ".txt";

    char * rutaFinal;
    char *rutaFinalIndice;
    if((rutaFinal = malloc(strlen(opcion)+strlen(ruta)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinal[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinal,opcion);
        //strcat(rutaFinal,ruta);
        strcat(rutaFinal,nombre_db);
        //strcat(rutaFinal,extension);
    } 
    if((rutaFinalIndice = malloc(strlen(opcion)+strlen(rutaIndices)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinalIndice[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinalIndice,rutaIndices);
        strcat(rutaFinalIndice,nombre_db);
        strcat(rutaFinalIndice,extension);
    }
    else {
        return 0;
    }
    conexion->id_sesion = 0;   
    
	int bytesSent = send(conexion->sockdf,rutaFinal,strlen(rutaFinal)+1,0); //enviamos la solicitud de crear una base al servidor
	if (bytesSent  < 0){
		printf("Error al enviar la solicitud de abrir base\n");
		return 0;
	}
	else{
		printf("Request enviado correctamente de abrir base\n");
	}
	
	char buf[100];
	int bytesRecieved = recv(conexion->sockdf, buf, 100, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta de abrir base\n");
		return 0;
	}
	else{
		printf("Respuesta recibida con exito\n");
		if(strcmp(buf,"error")==0){
			printf("No se pudo abrir la base de datos solicitada\n");
			return 0;
		
		}
	}

    //int serverStatus = send_all(conexion->sockdf, nombre_db, strlen(nombre_db), 0);

    free(rutaFinal);
    free(rutaFinalIndice);
    return 1;
}
