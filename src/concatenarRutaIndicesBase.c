#include <stdlib.h>
#include <string.h>

char * concatenarRutaIndicesBase(char * nombre_db, char * prefijo){
    char * rutaIndices = "./../bases/indice_";
    char * extension = ".txt";

    char *rutaFinalIndice;
    if((rutaFinalIndice = malloc(strlen(rutaIndices)+strlen(prefijo)+strlen(nombre_db)+strlen(extension)+1)) != NULL){
        rutaFinalIndice[0] = '\0';   // ensures the memory is an empty string
        strcat(rutaFinalIndice,rutaIndices);
        strcat(rutaFinalIndice, prefijo);
        strcat(rutaFinalIndice,nombre_db);
        strcat(rutaFinalIndice,extension);
    }
    
    return rutaFinalIndice;
}