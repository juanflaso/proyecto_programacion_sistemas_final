#include "logdb.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#define BUF_LEN 100


char *get_val(conexionlogdb *conexion, char *clave){
	int sockfd;
	sockfd= conexion->sockdf;
	char *valor = (char *)malloc(sizeof(BUF_LEN));
	if(valor==NULL)
		return NULL;
		
	int bytesSent = send(sockfd,clave,BUF_LEN,0); //enviamos la solicitud al servidor
	if (bytesSent  < 0){
		printf("Error al enviar la solicitud\n");
		return NULL;
	}
	else{
		printf("Request enviado correctamente\n");
	}
	char valor2[BUF_LEN];
	int bytesRecieved = recv(sockfd, valor2, BUF_LEN, 0);
	if (bytesRecieved  < 0){
		printf("Error al recibir la respuesta\n");
		return 0;
	}
	else{
		printf("Respuesta recibida con exito\n");
		if(strcmp(valor2,"error")==0){
			printf("No existe la clave\n");
			return NULL;
		
		}
	}
	return valor;
}