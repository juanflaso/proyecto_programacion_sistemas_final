#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"
void borrar(hashtable *tabla){
	int i,tamano;
	tamano = tabla->numeroBuckets;
	objeto **buckets = tabla->buckets;
	
	for(i=0 ; i < tamano ; i++){
		objeto *bucket;
		bucket=buckets[i];
		
		if (bucket != NULL){
			if ( bucket->siguiente == NULL){
				free(bucket->clave);
				bucket->clave = NULL;
				bucket->valor = NULL;
				free(bucket);
				bucket = NULL;
				tabla->elementos = tabla->elementos-1;
			}
			else{
				objeto *bucketTmp;
				
				while(bucket->siguiente != NULL){
					bucketTmp = bucket->siguiente;
					bucket->siguiente = bucketTmp->siguiente;
					free(bucketTmp->clave);
					bucketTmp->clave = NULL;
					bucketTmp->valor = NULL;
					bucketTmp->siguiente = NULL;
					free(bucketTmp);
					bucketTmp = NULL;
					tabla->elementos = tabla->elementos-1;
				}
				free(bucket->clave);
				bucket->clave = NULL;
				bucket->valor = NULL;
				free(bucket);
				bucket = NULL;
				tabla->elementos = tabla->elementos-1;
			}
		}
    }
}