#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "logdb.h"
#include "helper.h"


void compactar(conexionlogdb *conexion) {
    char * rutaBase = concatenarRutaBase(conexion->nombredb, "");

    if(rutaBase == NULL)
        return;

    char * rutaBaseIndice = concatenarRutaIndicesBase(conexion->nombredb, "");

    if(rutaBaseIndice == NULL)
        return;

    char * rutaBaseTemporal = concatenarRutaBase(conexion->nombredb, "temporal_");

    if(rutaBaseTemporal == NULL)
        return;

    char * rutaBaseIndiceTemporal = concatenarRutaIndicesBase(conexion->nombredb, "temporal_");

    if(rutaBaseIndiceTemporal == NULL)
        return;

    conexion->id_sesion = 1;

    FILE * fpIndices, *fpBase,* fpIndicesTemporal, *fpBaseTemporal;
    char * line = NULL;
	char * indexLine = NULL;
    size_t len, indexLen = 0;
    ssize_t read, readIndex;

    fpIndices = fopen(rutaBaseIndice, "r");
    fpBase = fopen(rutaBase, "r");
    fpIndicesTemporal = fopen(rutaBaseIndiceTemporal, "w+");
    fpBaseTemporal = fopen(rutaBaseTemporal, "w+");
     
    if (fpIndices == NULL || fpBase == NULL || fpBaseTemporal == NULL || fpIndicesTemporal == NULL){
        conexion->id_sesion = 0;
        return;
    }
        
    

    int counter = 0;
	int index;
	char ** arr;
    while ((read = getline(&line, &len, fpIndices)) != -1) {
        arr = split(line, ":", 2);

        if(arr == NULL){
            conexion->id_sesion = 0;
            return;
        }

		index = atoi(arr[1]);
		fseek( fpBase, index, SEEK_SET );
        if((readIndex = getline(&indexLine, &indexLen, fpBase)) != -1) {
			fputs(indexLine, fpBaseTemporal);
			fprintf(fpIndicesTemporal,"%s:%d\n",arr[0], counter);
			counter +=readIndex;
		}

        free(arr);
    }

    fclose(fpBase);
    fclose(fpBaseTemporal);
    fclose(fpIndices);
    fclose(fpIndicesTemporal);

    copiarArchivo(rutaBaseTemporal, rutaBase);
    copiarArchivo(rutaBaseIndiceTemporal, rutaBaseIndice);

    send_all(conexion->sockdf, "COMPACTADO", strlen("COMPACTADO"), 0);

    if (line)
        free(line);
    
    if (indexLine)
        free(indexLine);

    free(rutaBase);
    free(rutaBaseIndice);
    free(rutaBaseTemporal);
    free(rutaBaseIndiceTemporal);

    

    conexion->id_sesion = 0;
}